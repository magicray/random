import hashlib, time, os, socket, html, uuid, re

LOGPATH = os.environ.get('CLOUD_LOGPATH', '/tmp/logs')

LOGGER = dict(
    path    = os.path.join(LOGPATH, 'default'),
    streams = dict(),
    logger  = 'default'
)

def set_app_name(appid):
    LOGGER['path'] = os.path.join(LOGPATH, appid)

def set_stream_name(logger='default', session='default'):
    utc_usec  = time.time()
    gmtime    = time.gmtime(utc_usec)
    utc, usec = int(utc_usec), int((utc_usec - int(utc_usec)) * 1000000)

    yymmdd        = time.strftime("%y%m%d", gmtime)
    HH            = time.strftime("%H", gmtime)
    rotation_time = (gmtime.tm_min // 5) * 5

    stream = LOGGER['streams'].get(logger, dict(logger=logger,
                                                session=session,
                                                log_fd=None,
                                                blob_fd=None,
                                                blob_size=0,
                                                uuid=None,
                                                rotation_time=-1))
    LOGGER['logger']          = logger
    LOGGER['streams'][logger] = stream

    if stream['rotation_time'] >= rotation_time:
        return

    stream['rotation_time'] = rotation_time
    stream['uuid']          = uuid.uuid4().hex
    stream['blob_size']     = 0
    stream['href_pattern']  = re.compile("%s-([^-]*)-([^-]*)-%s" % (
        stream['uuid'], stream['uuid']))

    if stream['log_fd']  is not None: os.close(stream['log_fd'])
    if stream['blob_fd'] is not None: os.close(stream['blob_fd'])

    md5hash  = hashlib.md5(logger.encode('utf8')).hexdigest()[0:3]
    logdir   = os.path.join('streams', yymmdd, HH, md5hash, logger)
    indexdir = os.path.join('index', yymmdd)

    if os.path.isdir(os.path.join(LOGGER['path'], logdir)) is False:
        os.makedirs(os.path.join(LOGGER['path'], logdir))

    if os.path.isdir(os.path.join(LOGGER['path'], indexdir)) is False:
        os.makedirs(os.path.join(LOGGER['path'], indexdir))

    hostname = socket.gethostname()
    filemode = os.O_CREAT|os.O_WRONLY|os.O_APPEND
    filepath = os.path.join(logdir, '.'.join([stream['session'],
                                       yymmdd + HH + "%02d" % rotation_time,
                                       str(os.getpid()), hostname]))

    index_fd = os.open(os.path.join(LOGGER['path'], indexdir, hostname),
                       filemode)
    os.write(index_fd, (filepath + '\n').encode('utf8'))
    os.close(index_fd)

    filepath          = os.path.join(LOGGER['path'], filepath)
    stream['log_fd']  = os.open(filepath, filemode)
    stream['blob_fd'] = os.open(filepath + '.blob', filemode)

def log(msg):
    utc_usec  = time.time()
    gmtime    = time.gmtime(utc_usec)
    utc, usec = int(utc_usec), int((utc_usec - int(utc_usec)) * 1000000)

    timestamp     = time.strftime("%y%m%d.%H%M%S.", gmtime) + str(usec)
    rotation_time = (gmtime.tm_min // 5) * 5

    logger  = LOGGER['logger']
    stream  = LOGGER['streams'][logger]
    pattern = stream['href_pattern']

    if rotation_time > stream['rotation_time']:
        set_stream_name(stream['logger'], stream['session'])

    log_msg = html.escape(msg.split('\n')[0])
    log_msg = "<tr><td>%s</td><td>%s</td></tr>\n" % (timestamp, log_msg)

    log_msg = pattern.sub("<a href=\"?p=\\1&l=\\2\">link</a>", log_msg)

    os.write(stream['log_fd'], log_msg.encode('utf8'))

def dump(data):
    logger    = LOGGER['logger']
    stream    = LOGGER['streams'][logger]
    blob_size = stream['blob_size']
    guid      = stream['uuid']

    if type(data) == str:
        data = data.encode('utf8')
    else:
        data = str(data).encode('utf8')

    len_data  = len(data)

    os.write(stream['blob_fd'], data)

    stream['blob_size'] = blob_size + len_data
    return "%s-%d-%d-%s" % (guid, blob_size, len_data, guid)
