#!/bin/bash -e

CORE="linux-image-2.6.32-5-amd64 linux-headers-2.6.32-5-amd64                 \
    linux-doc-2.6.32                                                          \
                                                                              \
    grub-legacy acpi laptop-mode-tools lshw ndisgtk busybox-static            \
    sudo schroot debootstrap makedev inotify-tools sysstat procinfo           \
    alsa-utils                                                                \
                                                                              \
    kvm qemu libvirt-bin                                                      \
    virtualbox-ose-qt virtualbox-ose-dkms lxc user-mode-linux                 \
                                                                              \
    pmount hdparm sdparm aoetools cryptsetup avfs encfs sshfs                 \
    mdadm lvm2 dosfstools xfsprogs jfsutils reiserfsprogs ntfs-3g             \
    gddrescue squashfs-tools gpart gparted parted partimage                   \
                                                                              \
    p7zip-full lzma grsync rdiff rdiff-backup duplicity unison whois          \
    screen minicom gtkterm sqlite3 db4.6-util uuid expect                     \
    htop less extract fabric libxml2-utils                                    \
                                                                              \
    ssh ebtables xinetd stunnel ntp snmp ppp openvpn vde2 vpnc                \
    ufw tcpdump socat nmap ngrep iftop iperf iptraf host traceroute tshark    \
    bittorrent telnet ftp lftp curl mutt inadyn netcat-openbsd lynx           \
    dnsutils ifplugd wpagui wifi-radar ethtool                                \
    arpwatch snort wireshark kismet                                           \
                                                                              \
    daemontools supervisor nginx thttpd dnsmasq rails mongrel gunicorn        \
    rabbitmq-server asterisk gearman beanstalkd jetty                         \
    memcached memcachedb couchdb rails                                        \
                                                                              \
    libapache2-mod-php5 libapache2-mod-python libapache2-mod-perl2            \
    pgpool2 slony1-bin mysql-server-5.1 mysql-client-5.1 mysql-proxy          \
    squid varnish postfix fetchmail procmail getmail4                         \
                                                                              \
    ia32-libs ia32-libs-gtk lib32asound2-plugins                              \
                                                                              \
    build-essential elfutils nasm swig                                        \
    openjdk-6-jdk scala libnetty-java                                         \
    python3-* php5-cli erlang lua                                             \
    python-pylons python-django python-twisted python-turbogears2             \
    subversion-tools cvs rcs bzrtools git                                     \
    eclipse vim idle tkdiff jedit                                             \
                                                                              \
    vlc smplayer ffmpeg gnash gnash-tools feh imagemagick crip                \
    wodim genisoimage dvd+rw-tools dvdbackup cdrdao cdparanoia vcdimager      \
    cdtool eject                                                              \
                                                                              \
    icedove iceweasel iceweasel-firebug filezilla transmission                \
    icedtea6-plugin mozilla-plugin-gnash ttf-indic-fonts                      \
    openoffice.org lyx gv xpdf xpdf-utils pdfchain                            \
    gimp gpicview mtpaint gpaint graphviz                                     \
    zim xpad xournal                                                          \
                                                                              \
    xorg tightvncserver xtightvncviewer xrdp rdesktop                         \
    lxrandr arandr sakura xterm scrot                                         \
    tint2 openbox fbdesk fluxbox"

DESKTOP="firmware-linux zd1211-firmware atmel-firmware firmware-realtek       \
    firmware-atheros                                                          \
    firmware-ipw2x00 firmware-iwlwifi firmware-ralink libertas-firmware       \
                                                                              \
    grip acidrip dvdrip avidemux vcdimager w64codecs libdvdcss2               \
    bluez modemmanager cups                                                   \
                                                                              \
    iceowl google-chrome-stable pidgin ekiga xchat deluge                     \
    flashplugin-nonfree                                                       \
    evince abiword gnumeric gnucash tomboy dia scribus                        \
    shotwell cheese picasa                                                    \
                                                                              \
    gdm3 grandr file-roller gcalctool shutter                                 \
    gnome-screensaver gvfs-backends gnome-applets gnome-media gnome-bluetooth \
    gnome-system-monitor gnome-control-center gnome-terminal brasero gedit    \
    network-manager-gnome mobile-broadband-provider-info                      \
    network-manager-vpnc-gnome network-manager-openvpn-gnome                  \
    desktop-base gnome-backgrounds gnome-themes gnome-session"

IDIR=$PWD/idir && ETC=$IDIR/etc && VERSION=SQUEEZE_$(date +%d%b%y | tr a-z A-Z)
################################################################################
function apt_install {
    chroot $IDIR apt-get update
    chroot $IDIR apt-get dist-upgrade
    chroot $IDIR apt-get --no-install-recommends install $1
    chroot $IDIR apt-get clean
}

function makesquashfs {
    find $IDIR/var/cache/apt -type f -exec rm -f {} \;
    find $IDIR/var/log       -type f -exec rm -f {} \;
    rm -f $IDIR/etc/X11/xorg.conf $IDIR/etc/hostname
    $IDIR/usr/bin/mksquashfs $IDIR $1 -e proc sys boot
    chmod 444 $1
}

################################################################################
debootstrap --arch amd64 squeeze $IDIR && cp $0 $IDIR/bin
mount -t proc none $IDIR/proc
mount -t sysfs none $IDIR/sys
mount -t devpts none $IDIR/dev/pts

cat << "EO_SRC_LIST" > $IDIR/etc/apt/sources.list && apt_install "$CORE"
deb http://http.us.debian.org/debian squeeze main
deb http://security.debian.org/ squeeze/updates main
EO_SRC_LIST

P=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
cp $IDIR/usr/share/zoneinfo/Asia/Calcutta                $ETC/localtime
echo "export PATH=$P:/home/opt/bin:/home/opt/sbin"     >>$ETC/profile
echo -e "auto lo\niface lo inet loopback\n"             >$ETC/network/interfaces
for i in $(seq 0 9);do echo iface eth$i inet dhcp;done >>$ETC/network/interfaces
echo "append domain-name-servers 8.8.8.8;"             >>$ETC/dhcp/dhclient.conf
rm $IDIR/etc/mtab && ln -s /proc/mounts $IDIR/etc/mtab

sed -i 's|root:[^:]*:|root:!:|'                           $ETC/shadow
sed -i '/^PermitRootLogin yes/s/^.*$/PermitRootLogin no/' $ETC/ssh/sshd_config
echo "DenyUsers guest"                                 >> $ETC/ssh/sshd_config

sed -i '/^DIR_MODE/s|^.*$|DIR_MODE=0700|'                    $ETC/adduser.conf
sed -i '/^#ADD_EXTRA_GROUPS/s|^#||'                          $ETC/adduser.conf
sed -i '/^#EXTRA_GROUPS=/s|^#||'                             $ETC/adduser.conf
sed -i '/^EXTRA_GROUPS=/s|"\w*$| netdev powerdev kvm junk"|' $ETC/adduser.conf
sed -i '/^EXTRA_GROUPS=/s|"|"junk |'                         $ETC/adduser.conf

echo 'export PATH=$HOME/local/bin:$HOME/local/sbin:$PATH' >> $ETC/skel/.profile
echo 'export EDITOR=vim'                                  >> $ETC/skel/.profile
echo -e "syntax on\nset hlsearch"                         >> $ETC/skel/.vimrc
echo "startfluxbox"                                        > $ETC/skel/.xinitrc

mkdir $ETC/skel/.fluxbox
cat << "EO_FLUXBOX_INIT" > $ETC/skel/.fluxbox/init
session.menuFile: ~/.fluxbox/menu
session.screen0.toolbar.widthPercent: 90
session.screen0.strftimeFormat: %d %b, %a %02k:%M
session.screen0.toolbar.tools: prevworkspace, workspacename, nextworkspace, iconbar, systemtray, clock
session.styleFile: /usr/share/fluxbox/styles/MerleyKay
EO_FLUXBOX_INIT

for i in 5001:vault 5002:opt 6000:guest; do
    I=$(echo $i | cut -d: -f1)
    U=$(echo $i | cut -d: -f2)
    chroot $IDIR adduser --disabled-password --uid $I --gecos "" $U
done

sed -i '/^INTERFACES/s/^.*$/INTERFACES="auto"/'      $ETC/default/ifplugd
sed -i '/^HOTPLUG_/s/^.*$/HOTPLUG_INTERFACES="all"/' $ETC/default/ifplugd

DAEMONS="kdm dnsmasq rsync xinetd squid thttpd lighttpd nginx arpwatch        \
    memcached memcachedb mysql mysql-proxy couchdb postgresql pgpool2 slony1  \
    rsyslog schroot stunnel4 nfs-common portmap uml-utilities lvm2 nagios3    \
    ufw autofs gearman-job-server rabbitmq-server snort asterisk apache2      \
    avahi-daemon laptop-mode fetchmail postfix mdadm-raid mdadm supervisor    \
    xrdp openvpn varnish beanstalkd jetty"
for i in $DAEMONS; do chroot $IDIR update-rc.d -f $i remove; done

makesquashfs ${VERSION}_CORE.sqfs
################################################################################
cat << "EO_SRC_LIST" > $IDIR/etc/apt/sources.list && apt_install "$DESKTOP"
deb http://http.us.debian.org/debian squeeze main non-free contrib
deb http://security.debian.org/ squeeze/updates main non-free contrib
deb http://www.debian-multimedia.org squeeze main non-free
deb http://dl.google.com/linux/deb/ stable main non-free
EO_SRC_LIST

GREETER=$ETC/gdm3/greeter.gconf-defaults
echo "/apps/gdm/simple-greeter/disable_user_list true" >> $GREETER

wget http://linux.dropbox.com/packages/debian/nautilus-dropbox_0.7.1_amd64.deb
mv nautilus-dropbox_0.7.1_amd64.deb $IDIR
chroot $IDIR dpkg -i nautilus-dropbox_0.7.1_amd64.deb

for i in $DAEMONS; do chroot $IDIR update-rc.d -f $i remove; done

makesquashfs ${VERSION}_DESKTOP.sqfs
################################################################################
rm -rf initrd final $VERSION.iso && mkdir -p final/boot
mkdir -p initrd/{proc,sys,home,media,selinux,bin,lib,lib64}
mkdir -p initrd/dev/pts initrd/{rw,sqfs,tmpfs}

cp -a $IDIR/lib/modules                           initrd/lib
cp $IDIR/lib/libc.so.6                            initrd/lib
cp $IDIR/lib64/ld-linux-x86-64.so.2               initrd/lib64
cp $IDIR/bin/busybox $IDIR/sbin/{modprobe,insmod} initrd/bin

(cd initrd/dev && MAKEDEV generic)

sed "s|VERSION|$VERSION|g" << "EO_INIT" > initrd/init && chmod 755 initrd/init
#!/bin/busybox ash

clear
mount -t proc  proc  /proc && echo "Mounted /proc"
mount -t sysfs sysfs /sys  && echo "Mounted /sys"
echo 0 > /proc/sys/kernel/printk

GRUB_OPT=$(cat /proc/cmdline | awk '{print $2}')
[ $GRUB_OPT = "BusyBox" ] && exec ash

rm -r /lib/modules/2.6.32-5-amd64/kernel/drivers/video
modprobe -a isofs ext3 ext4 jfs xfs reiserfs fuse

echo -n "Detecting hardware "; for i in 1 2 3 4 5 6 7 8 9; do
    modprobe -a $(find /sys/devices -name modalias -exec cat {} \;)
    sleep 1; echo -n "."
done 2> /dev/null; echo

for d in $(cat /proc/diskstats | awk '{print $3}'); do
    mkdir /media/$d && mount -o ro /dev/$d /media/$d
    [ $? = 0 ] && echo "Mounted /media/$d (ro)" || rmdir /media/$d

    D=/media/$d/boot

    if [ $GRUB_OPT = "Run" ]; then
        [ -d $D/home ]                 && CDIR=$D
        [ -f $D/root ]                 && CDIR=$D
        [ -f $D/hostname ]             && CDIR=$D
        [ -f $D/VERSION_CORE.sqfs ]    && SQFS=$D/VERSION_CORE.sqfs
        [ -f $D/VERSION_DESKTOP.sqfs ] && SQFS=$D/VERSION_DESKTOP.sqfs
    else
        [ -f $D/VERSION_DESKTOP.sqfs ] && SQFS=$D/VERSION_DESKTOP.sqfs
        [ -f $D/VERSION_CORE.sqfs ]    && SQFS=$D/VERSION_CORE.sqfs
    fi
done 2> /dev/null

[ -f $CDIR/VERSION_CORE.sqfs ]    && SQFS=$CDIR/VERSION_CORE.sqfs
[ -f $CDIR/VERSION_DESKTOP.sqfs ] && SQFS=$CDIR/VERSION_DESKTOP.sqfs

[ $SQFS ] || exec ash

modprobe -a loop squashfs aufs
mount -o loop -t squashfs $SQFS /sqfs
mount -t tmpfs tmpfs /tmpfs && chmod 755 /tmpfs && mkdir /tmpfs/delta
mount -t aufs -o dirs=/tmpfs/delta=rw:/sqfs=ro none /rw
echo "Mounted $SQFS"

cd $CDIR
for d in $(ls /sys/block); do
    if [ "x1" = x$(cat /sys/block/$d/removable) ]; then
        for p in $(ls -d /media/$d*); do
            umount $p && echo "Unmounted $p" && rmdir $p
        done
    fi
done 2> /dev/null
cd /

for d in lib lib32 lib64 bin sbin usr etc var opt; do
    rm -rf $d
    ln -s /rw/$d $d
done

HOSTNAME=$(dmidecode | md5sum | cut -c 1-8)
rmmod $(lsmod | grep -v ehci_hcd) >& /dev/null

if [ $CDIR ]; then
    for d in $(ls /media); do
        mount -o remount,rw,nosuid,noatime,nodiratime /media/$d
        [ $? = 0 ] && echo "Mounted /media/$d (rw)"
    done 2> /dev/null

    mkdir -p $CDIR/tmp && chmod 1777 $CDIR/tmp && ln -s $CDIR/tmp /tmp

    d=$(date +%y%m%d_%H%M%S) && mkdir -p $CDIR/log
    cp -a /var/log $CDIR/log/$d && rm -rf /var/log
    ln -s $CDIR/log/$d /var/log

    [ -d $CDIR/home ] && mount --bind $CDIR/home /home
    for U in $(ls /home); do
        I=$(stat -c %g /home/$U)
        P=$(cat /home/$U/.password)
        /usr/sbin/adduser --disabled-password --uid $I --gecos "" $U
        usermod -p $P $U
    done >& /dev/null

    usermod -p $(cat $CDIR/root 2> /dev/null) root >& /dev/null

    [ -f $CDIR/hostname ]  && HOSTNAME=$(cat $CDIR/hostname)
    [ -f $CDIR/autologin ] && AUTOLOGIN=$(cat $CDIR/autologin)
else
    mkdir /tmp && mount -t tmpfs tmpfs /tmp && chmod 1777 /tmp

    [ $GRUB_OPT = "Run" ] || echo -e "root\nroot" | passwd root  >&/dev/null

    AUTOLOGIN=guest && cp -a /sqfs/home/$AUTOLOGIN /home/$AUTOLOGIN
    echo -e "$AUTOLOGIN\n$AUTOLOGIN" | passwd $AUTOLOGIN >&/dev/null
fi

hostname $HOSTNAME

if [ $AUTOLOGIN ] && [ -d /home/$AUTOLOGIN ]; then
    GDM3=/etc/gdm3/daemon.conf
    sed -i "/# AutomaticLoginEnable/s|^.*$|AutomaticLoginEnable=true|" $GDM3
    sed -i "/# AutomaticLogin/s|^.*$|AutomaticLogin=$AUTOLOGIN|"       $GDM3
fi >& /dev/null

exec /sbin/init
EO_INIT

(cd initrd; find . | cpio -o -H newc | gzip > ../final/boot/$VERSION.initrd)
ln $IDIR/boot/vmlinuz-*    final/boot/$VERSION.kernel
ln ${VERSION}_CORE.sqfs    final/boot/${VERSION}_CORE.sqfs
ln ${VERSION}_DESKTOP.sqfs final/boot/${VERSION}_DESKTOP.sqfs

sed "s|VERSION|$VERSION|g" << "EO_MKISO" > final/boot/mkiso
#!/bin/bash

ARGS=($@)
for((i=1; i<$#; i++)); do A=${ARGS[$i]}; G="$G /$(basename $A)=$A "; done

TMPDIR=$(mktemp -d) && SQFSDIR=$(dirname $0) && mkdir $TMPDIR/boot

if [ -f $SQFSDIR/VERSION_CORE.sqfs ]; then
    CORE="/boot/VERSION_CORE.sqfs=$SQFSDIR/VERSION_CORE.sqfs"
fi

if [ -f $SQFSDIR/VERSION_DESKTOP.sqfs ]; then
    DESKTOP="/boot/VERSION_DESKTOP.sqfs=$SQFSDIR/VERSION_DESKTOP.sqfs"
fi

cp -r $IDIR/usr/lib/grub/x86_64-pc $TMPDIR/boot/grub && cp $0 $TMPDIR/boot

MENULST=$TMPDIR/boot/grub/menu.lst && echo "timeout 2" > $MENULST
echo "title Run     VERSION"                          >> $MENULST
echo "    kernel /boot/VERSION.kernel quiet Run"      >> $MENULST
echo "    initrd /boot/VERSION.initrd"                >> $MENULST
echo "title Admin   VERSION"                          >> $MENULST
echo "    kernel /boot/VERSION.kernel quiet Admin"    >> $MENULST
echo "    initrd /boot/VERSION.initrd"                >> $MENULST
echo "title BusyBox VERSION"                          >> $MENULST
echo "    kernel /boot/VERSION.kernel quiet BusyBox"  >> $MENULST
echo "    initrd /boot/VERSION.initrd"                >> $MENULST

[ -f $SQFSDIR/VERSION.iso ] && ISO="/boot/VERSION.iso=$SQFSDIR/VERSION.iso"

genisoimage -r -b boot/grub/stage2_eltorito -no-emul-boot       \
    -boot-load-size 4 -boot-info-table -o $1 -graft-point       \
    /boot/grub=$TMPDIR/boot/grub                                \
    /boot/VERSION.initrd=$SQFSDIR/VERSION.initrd                \
    /boot/VERSION.kernel=$SQFSDIR/VERSION.kernel                \
    /boot/mkiso=$SQFSDIR/mkiso $G $ISO $CORE $DESKTOP
EO_MKISO

chmod -R 644 final/boot && chmod 755 final/boot final/boot/mkiso
final/boot/mkiso $VERSION.iso
