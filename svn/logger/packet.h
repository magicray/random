#ifndef __PACKET_H__
#define __PACKET_H__

#include <stdint.h>
#include <limits.h>

#include "client.h"

#define MAX_EVENT (8)

char* packet_connection_recv(void *conn, uint32_t *type, uint32_t *length);
int packet_connection_send(void *conn, uint32_t type, uint32_t length);
char* packet_connection_allocate_send_buf(void *conn, uint32_t length);

void packet_handler_processing(void* conn);
void packet_handler_done(void *conn);
void packet_handler_abort(void *conn);

void packet_trigger_event(int32_t);
void packet_wait_event(void*, int32_t);

void packet_loop_run(int32_t, int32_t state_size);

void* packet_connect(const char*, int, uint32_t, uint32_t, char*);
#endif
