#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <time.h>
#include <string.h>

#include "debug.h"

#define SIZE (256*1024*1024)

static struct logger_context {
    const char *name;
    int   size;
    int   fd;
    char *buf;
} context[] = {{"debug", SIZE, -1}, {"alert", SIZE, -1}};

void mmap_log(int type, const char *file, int line, const char *fmt, ...) {
    char newpath[128], oldpath[128];

    for(int i=0; i<=type; i++) {
        struct logger_context *ctx = &context[i];

        if(ctx->size > (SIZE - 10*1024)) {
            sprintf(oldpath, "log/%s.old", ctx->name);
            sprintf(newpath, "log/%s",     ctx->name);

            if(ctx->fd >= 0) {
                munmap(ctx->buf, SIZE);
                ftruncate(ctx->fd, ctx->size);
                close(ctx->fd);
            }
            rename(newpath, oldpath);
            ctx->size = 0;
        }

        if(0 == ctx->size) {
            sprintf(newpath, "log/%s", ctx->name);
            ctx->fd = open(newpath, O_RDWR|O_CREAT|O_TRUNC, 0644);
            ftruncate(ctx->fd, SIZE);
            ctx->buf = (char*)mmap(0, SIZE, PROT_WRITE, MAP_SHARED, ctx->fd, 0);
        }
    }

    va_list ap;
    struct  timespec t;
    char    buffer[8096];
    clock_gettime(CLOCK_REALTIME, &t);

    int32_t n = sprintf(buffer,
                        "%d.%06d %s %03d : ",
                        t.tv_sec, t.tv_nsec/1000, file, line);

    va_start(ap, fmt);
    n += vsnprintf(buffer+n, 8000, fmt, ap);
    va_end(ap);

    buffer[n]   = '\n';
    buffer[n+1] = 0;

    for(int i=0; i<=type; i++) {
        struct logger_context *ctx = &context[i];

        memcpy(ctx->buf + ctx->size, buffer, n+1);
        ctx->size += n+1;
    }
}
