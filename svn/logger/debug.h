#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdlib.h>
#include <stdint.h>

void mmap_log(int, const char*, int, const char*, ...);

#define LOG(...)   do { mmap_log(0, __FILE__, __LINE__, __VA_ARGS__); } while(0)
#define ALERT(...) do { mmap_log(1, __FILE__, __LINE__, __VA_ARGS__); } while(0)

#define EXIT(...)         do { ALERT( __VA_ARGS__); exit(1);       } while(0)
#define ASSERT(cond)      do { if(!(cond)) EXIT("assert failed");  } while(0)
#define ASSERTZERO(expr)  do { ASSERT(0 == (expr));                } while(0)
#define ASSERTPOS(expr)   do { ASSERT((expr) > 0);                 } while(0)
#define ASSERTNEG(expr)   do { ASSERT((expr) < 0);                 } while(0)
#define ASSERTFALSE(expr) do { ASSERT(!(expr));                    } while(0)

#endif
