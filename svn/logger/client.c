#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#include "client.h"

int socket_connect(const char *ip, int port) {
    struct sockaddr_in addr;
    int sock;

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return -1;

    addr.sin_family      = AF_INET;
    addr.sin_addr.s_addr = inet_addr(ip);
    addr.sin_port        = htons(port);

    if(connect(sock, (struct sockaddr*)&addr, sizeof(addr))) {
        close(sock);
        return -2;
    }

    return sock;
}

uint32_t socket_send(int sock, uint32_t type, uint32_t length, char *buffer) {
    char typelen[8];
    uint32_t sent;

    *(uint32_t*)(&typelen[0]) = htobe32(type);
    *(uint32_t*)(&typelen[4]) = htobe32(length);

    sent = 0;
    while(sent < 8) {
        int count = write(sock, &typelen[0] + sent, 8 - sent);
        if(-1 == count)
            return 0;
        sent += count;
    }

    sent = 0;
    while(sent < length) {
        int count = write(sock, buffer + sent, length - sent);
        if(-1 == count)
            return sent;
        sent += count;
    }
    return sent;
}

char* socket_recv(int sock, uint32_t *type, uint32_t *length) {
    static char    *buffer = 0;
    static uint32_t buflen = 0;

    char typelen[8]; 

    *type = *length = 0;

    uint32_t received = 0;
    while(received < 8) {
        int count = read(sock, &typelen[0] + received, 8 - received);
        if(count < 1)
            return 0;
        received += count;
    }

    *type   = be32toh(*(uint32_t*)&typelen[0]);
    *length = be32toh(*(uint32_t*)&typelen[4]);

    if(buflen < *length) {
        free(buffer);
        buffer = (char*)malloc(*length);
        buflen = *length;
    }

    received = 0;
    while(received < *length) {
        int32_t count = read(sock, buffer + received, *length - received);
        if(count < 1)
            return 0;
        received += count;
    }

    return buffer;
}

int echo(int sock, char *in, char *out, uint32_t length) {
    if(length != socket_send(sock, REQ_ECHO, length, in))
        return -1;

    uint32_t type, len;
    char *p = socket_recv(sock, &type, &len);

    if(0 == p)
        return -2;

    if(len != length)
        return -3;

    memcpy(out, p, len);
    return 0;
}

int append(int sock, char *buffer, uint32_t length) {
    if(length != socket_send(sock, REQ_APPEND, length, buffer))
        return -1;
    else
        return 0;
}

int put(int sock, char *buffer, uint32_t length, char *id) {
    if(length != socket_send(sock, REQ_PUT, length, buffer))
        return -1;

    uint32_t type, len;
    char *p = socket_recv(sock, &type, &len);

    if(0 == p)
        return -2;

    if(12 != len)
        return -3;

    memcpy(id, p, len);

    return length;
}
