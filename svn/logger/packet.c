#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <sys/epoll.h>

#include "packet.h"
#include "client.h"
#include "debug.h"

static int epoll_socket;
static int state_size;

typedef struct {
    int32_t  socket;
    int64_t  in_bytes;
    int64_t  out_bytes;
    int64_t  in_packets;
    int64_t  out_packets;
    uint32_t status;

    uint32_t req_type;
    uint32_t req_length;
    char    *req_buffer;
    uint32_t req_buffer_length;
    uint32_t received;

    uint32_t rep_type;
    uint32_t rep_length;
    char    *rep_buffer;
    uint32_t rep_buffer_length;
    uint32_t sent;

    void    *state;
    void    *next[MAX_EVENT];
    void    *prev[MAX_EVENT];
} packet_connection;

static struct event_context {
    int32_t trigger;
    int32_t count;
    packet_connection* list;
} events[MAX_EVENT];

char* packet_connection_allocate_send_buf(void *ctx, uint32_t length) {
    packet_connection *conn = (packet_connection*)ctx;

    if(conn->rep_buffer_length < length) {
        free(conn->rep_buffer);
        conn->rep_buffer = (char*)malloc(length);
        conn->rep_buffer_length = length;
    }

    return conn->rep_buffer;
}

int packet_connection_send(void *ctx, uint32_t type, uint32_t length) {
    packet_connection *conn = (packet_connection*)ctx;

    conn->rep_type   = htobe32(type);
    conn->rep_length = htobe32(length);
    conn->sent       = 0;
    conn->status    |= EPOLLOUT;

    return 0;
}

char* packet_connection_recv(void *ctx, uint32_t *type, uint32_t *length) {
    packet_connection *conn = (packet_connection*)ctx;

    conn->status |= EPOLLIN;

    if(0 == conn->received)
        return 0;

    ASSERT(conn->received >= 8);

    *type   = be32toh(conn->req_type);
    *length = be32toh(conn->req_length);

    conn->received = 0;

    return conn->req_buffer;
}

void packet_handler_processing(void *conn) {
}

void packet_handler_done(void *conn) {
    ((packet_connection*)conn)->status |= 0x80000000;
}

void packet_handler_abort(void *conn) {
    ((packet_connection*)conn)->status |= 0x40000000;
}

void packet_trigger_event(int32_t event) {
    ASSERT(event < MAX_EVENT);

    events[event].trigger = 1;
}

void packet_wait_event(void *conn_ptr, int32_t event) {
    ASSERT(event < MAX_EVENT);

    packet_connection *conn = (packet_connection*)conn_ptr;

    if((0 != conn->next[event]) ||
       (0 != conn->prev[event]) ||
       (conn == events[event].list))
    {
        LOG("socket(%d) alread waiting on event(%d). total(%d)",
            conn->socket, event, events[event].count);
        return;
    }

    packet_connection *curr = events[event].list;

    conn->next[event] = curr;
    conn->prev[event] = 0;

    if(0 != curr)
        curr->prev[event] = conn;

    events[event].list   = conn;
    events[event].count += 1;

    LOG("socket(%d) waiting on event(%d). total(%d)",
        conn->socket, event, events[event].count);
}

void packet_handler(void *conn, void *conn_state);
void packet_loop_callback(void);

static uint32_t handler(packet_connection *conn, uint32_t events) {
    uint32_t next_events = 0;

    ASSERT(events == (events & (EPOLLIN|EPOLLOUT)));

#define WRITEBYTES(b, l) do {                                             \
    int n = write(conn->socket, (char*)(b) + conn->sent, l - conn->sent); \
    if(-1 == n) {                                                         \
        LOG("socket(%d) write failed", conn->socket);                     \
        return 0x40000000;                                                \
    }                                                                     \
    conn->sent      += n;                                                 \
    conn->out_bytes += n;                                                 \
} while(0)

    if(EPOLLOUT & events) {
        uint32_t out_type   = be32toh(conn->rep_type);
        uint32_t out_length = be32toh(conn->rep_length) + 8;

        if(conn->sent < 8)
            WRITEBYTES(&conn->rep_type, 8);

        if((conn->sent >= 8) && (out_length > 8))
            WRITEBYTES(conn->rep_buffer-8, out_length);

        ASSERT(conn->sent <= out_length);

        if(out_length == conn->sent) {
            conn->out_packets += 1;

            LOG("socket(%d) out(%d:%d) packets(%lu) bytes(%lu)",
                 conn->socket, out_type, out_length,
                 conn->out_packets, conn->out_bytes);
        } else
            next_events |= EPOLLOUT;
    }

#define READBYTES(b, l) do {                                                 \
    int n = read(conn->socket, (char*)(b)+conn->received, l-conn->received); \
    if(0 == n) {                                                             \
        LOG("socket(%d) received %d bytes", conn->socket, n);                \
        return 0x40000000;                                                   \
    }                                                                        \
    if(n > 0) {                                                              \
        conn->received += n;                                                 \
        conn->in_bytes += n;                                                 \
    }                                                                        \
} while(0)

    if(EPOLLIN & events) {
        if(conn->received < 8)
            READBYTES(&conn->req_type, 8);

        if((conn->received >= 8) && (be32toh(conn->req_length) > 0)) {
            uint32_t length = be32toh(conn->req_length);

            if(conn->req_buffer_length < length) {
                free(conn->req_buffer);
                conn->req_buffer = (char*)malloc(length);
                conn->req_buffer_length = length;
            }

            READBYTES(conn->req_buffer-8, length+8);
        }

        long total = 8 + ((conn->received >= 8) ?
                          be32toh(conn->req_length) :
                          UINT_MAX);

        ASSERT(conn->received <= total);

        if(total == conn->received) {
            conn->in_packets += 1;

            LOG("socket(%d) in(%d:%d) packets(%lu) bytes(%lu)", conn->socket,
                 be32toh(conn->req_type), be32toh(conn->req_length),
                 conn->in_packets, conn->in_bytes);
        } else
            next_events |= EPOLLIN;
    }

    if(next_events & (EPOLLIN|EPOLLOUT))
        return next_events;

    conn->status = 0;
    packet_handler(conn, conn->state);
    return conn->status;
}

void packet_loop_run(int32_t port, int32_t in_state_size) {
    int32_t sockfd;
    struct  sockaddr_in server_addr;
    struct  epoll_event ep_event;

    state_size = in_state_size;

    ALERT("port(%d) state_size(%d)", port, state_size);
    bzero(events, sizeof(events));

    ASSERTPOS(sockfd = socket(AF_INET, SOCK_STREAM, 0));

    server_addr.sin_family      = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port        = htons(port);

    ASSERTZERO(bind(sockfd,(struct sockaddr*)&server_addr,sizeof(server_addr)));

    uint32_t flags = fcntl(sockfd, F_GETFL, 0); 
    ASSERTZERO(fcntl(sockfd, F_SETFL, flags | O_NONBLOCK));

    ASSERTZERO(listen(sockfd, 32));

    ASSERTPOS(epoll_socket = epoll_create(1));

    ep_event.events   = EPOLLIN;
    ep_event.data.ptr = 0;
    ASSERTZERO(epoll_ctl(epoll_socket, EPOLL_CTL_ADD, sockfd, &ep_event));

    while(1) {
        struct epoll_event epoll_event_array[100000];

        int count = epoll_wait(epoll_socket, epoll_event_array, 100000, 1000);

        for(int i=0; i<count; i++) {
            uint32_t  events  = epoll_event_array[i].events;
            void     *context = epoll_event_array[i].data.ptr;

            if(0 == context) {
                struct epoll_event e;
                struct sockaddr_in addr;
                socklen_t addrlen = sizeof(addr);

                int32_t fd = accept(sockfd, (struct sockaddr*)&addr, &addrlen);
                if(-1 == fd) {
                    LOG("accept failed on socket fd(%d), events(%08X)",
                        sockfd, events);
                    continue;
                }
                LOG("socket(%d) connection receieved", fd);

                int32_t flags = fcntl(fd, F_GETFL, 0); 
                ASSERTZERO(fcntl(fd, F_SETFL, flags | O_NONBLOCK));

                void *c = calloc(sizeof(packet_connection), 1);
                void *s = calloc(state_size, 1);

                if((0 == c) || (0 == s)) {
                    ASSERTZERO(close(fd));
                    free(c);
                    free(s);
                    LOG("socketfd(%d) malloc failed", fd);
                    continue;
                }

                ((packet_connection*)c)->socket = fd;
                ((packet_connection*)c)->state  = s;

                e.events   = EPOLLIN;
                e.data.ptr = c;
                ASSERTZERO(epoll_ctl(epoll_socket, EPOLL_CTL_ADD, fd, &e));
            }
#define CLOSE(CONN) do {                                                 \
    packet_connection *conn = (packet_connection*)(CONN);                \
    conn->received   = 8;                                                \
    conn->req_type   = htobe32(CONNECTION_CLOSED);                       \
    conn->req_length = 0;                                                \
    packet_handler(conn, conn->state);                                   \
    ASSERTZERO(epoll_ctl(epoll_socket, EPOLL_CTL_DEL, conn->socket, 0)); \
    ASSERTZERO(close(conn->socket));                                     \
    free(conn->rep_buffer);                                              \
    free(conn->state);                                                   \
    LOG("socket(%d) connection closed", conn->socket);                   \
    conn->socket = -1;                                                   \
} while(0)

#define EPOLL_MOD(ev, conn) do {                                          \
    struct epoll_event e;                                                 \
    e.events   = ev;                                                      \
    e.data.ptr = conn;                                                    \
    ASSERTZERO(epoll_ctl(epoll_socket, EPOLL_CTL_MOD, conn->socket, &e)); \
} while(0)
            else if(events == (events & (EPOLLIN|EPOLLOUT))) {
                packet_connection *c = (packet_connection*)context;
                uint32_t result = handler(c, events);
                if(result > 0x00FFFFFF)
                    CLOSE(c);
                else
                    EPOLL_MOD(result, c);
            } else
                CLOSE(context);
        }

        for(int i=0; i<MAX_EVENT; i++) {
            if(1 == events[i].trigger) {
                struct event_context e = events[i];
                bzero(&events[i], sizeof(struct event_context));

                while(0 != e.list) {
                    packet_connection *conn = e.list;
                    e.list = (packet_connection*)conn->next[i];
                    conn->next[i] = 0;
                    conn->prev[i] = 0;

                    if(conn->socket >= 0) {
                        LOG("socket(%d) handling event(%d)", conn->socket, i);

                        conn->status = 0;
                        packet_handler(conn, conn->state);

                        if(conn->status > 0x00FFFFFF)
                            CLOSE(conn);
                        else
                            EPOLL_MOD(conn->status, conn);
                    }
                }
            }
        }

        packet_loop_callback();
    }
}

void* packet_connect(const char *ip,
                     int      port,
                     uint32_t type,
                     uint32_t length,
                     char    *buffer)
{
    struct epoll_event e;
    int sock;
    int flags;
    
    if((sock = socket_connect(ip, port)) < 0)
        return 0;

    flags = fcntl(sock, F_GETFL, 0);
    ASSERTZERO(fcntl(sock, F_SETFL, flags | O_NONBLOCK));

    void *c = calloc(sizeof(packet_connection), 1);
    void *s = calloc(state_size, 1);

    if((0 == c) || (0 == s)) {
        ASSERTZERO(close(sock));
        free(c);
        free(s);
        LOG("socketfd(%d) malloc failed", sock);
        return 0;
    }

    ((packet_connection*)c)->socket = sock;
    ((packet_connection*)c)->state  = s;

    e.events   = EPOLLIN;
    e.data.ptr = c;

    ASSERTZERO(epoll_ctl(epoll_socket, EPOLL_CTL_ADD, sock, &e));

    if(length != socket_send(sock, type, length, buffer)) {
        ASSERTZERO(close(sock));
        free(c);
        free(s);
        LOG("socketfd(%d) malloc failed", sock);
        return 0;
    }

    return s;
}
