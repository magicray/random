import utils, sqlite3, sys, time

sql = utils.SQLClient(sqlite3.connect('stocks.db'))

sql.add("get_indicators",
        "select * from indicators where stock=:stock and date=:date")

def reco(symbol, date):
    info   = sql.get_indicators(stock=symbol, date=date)
    print(info)
    test_1 = info['ma1']   > info['ma2']
    test_2 = True
    test_2 = info['close'] > info['range_high']
    test_3 = True
    test_3 = info['roc']   < 120
    test_4 = True
    test_4 = info['roc']   > 110
    test_5 = True
    test_5 = info['rsi']   > 50

    if True == (test_1 and test_2 and test_3 and test_4 and test_5):
        return 'BUY'

class Portfolio(object):
    def __init__(self, cash):
        self.seed_cash      = cash
        self.available_cash = cash
        self.longs     = dict()

    def networth(self, date):
        networth = self.available_cash

        for s in self.longs:
            try: close = stock.cache.get(s).infoD[date]['close']
            except: continue
            for p in self.longs[s]:
                networth += (close * p['quantity']) * 0.99

        return networth

    def trade_amount(self, date):
        amount = self.networth(date) / 10
        return amount if (amount < self.available_cash) else self.available_cash

    def long_open(self, symbol, date):
        close    = stock.cache.get(symbol).infoD[date]['close']
        quantity = self.trade_amount(date) // close

        if 0 == quantity:
            return False

        amount   = quantity * close

        if (self.available_cash - (amount * 1.02)) >= 0:
            self.available_cash -= amount * 1.01
        else:
            return False

        if symbol not in self.longs.keys():
            self.longs[symbol] = list()

        self.longs[symbol].append(dict(date=date,quantity=quantity,price=close))
        return (close, quantity, amount)

    def long_close(self, symbol, position, date, reason):
        quantity = position['quantity']
        price    = stock.cache.get(symbol).infoD[date]['close']

        self.available_cash += (quantity*price) * 0.99
        self.longs[symbol].remove(position)

        val = (date, price, quantity, quantity * price, reason)
        print("%d %8.2f %4d %8.2f %s" % val)


    def buy(self, date):
        for s in symbol_list:
            try: close = stock.cache.get(s).infoD[date]['close']
            except: continue
            if 'BUY' == reco(s, date):
                n = len(self.longs[s]) if s in self.longs.keys() else 0
                if n > 0:
                    last_buy_date = str(self.longs[s][-1]['date'])
                    last_buy_date = time.strptime(last_buy_date, "%Y%m%d")
                    last_buy_date = time.mktime(last_buy_date)
                    today_date    = time.strptime(str(date), "%Y%m%d")
                    today_date    = time.mktime(today_date)
                    day_diff      = (today_date - last_buy_date) // 86400
                    if day_diff < 10:
                        pass#continue

                tup = self.long_open(s, date)
                if tup is not False:
                    print("%d %8.2f %4d %8.2f BUY"%(date,tup[0],tup[1],tup[2]))

    def sell(self, date):
        for s in self.longs.keys():
            try: info = stock.cache.get(s).infoD[date]
            except: continue
            for p in self.longs[s]:
                if info['close'] > 1.25 * p['price']:
                    self.long_close(s, p, date, 'SELL')
                #elif info['ma1'] < info['ma2']:
                #    self.long_close(s, p, date, 'STOP MA')
                elif info['close'] < 0.75 * p['price']:
                    self.long_close(s, p, date, 'STOP')

if __name__ == '__main__':
    symbol_list = sys.argv[1:]
    portfolio = Portfolio(100000)
    networth = 100000
    cash     = 100000
    for d in sorted([p['date'] for p in stock.cache.get('mtnl').infoA]):
        portfolio.sell(d)
        portfolio.buy(d)
        if cash != portfolio.available_cash:
            networth = portfolio.networth(d)
            cash     = portfolio.available_cash
            profit = portfolio.networth(d) - portfolio.seed_cash
            val = (d, portfolio.networth(d), portfolio.available_cash, profit)
            print('%d %8.2f %8.2f %8.2f' % val)
    print(portfolio.longs)
