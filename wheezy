#!/bin/bash -e

APT_PROXY=127.0.0.1:3142
VERSION=WHEEZY_$(date +%d%b%y | tr a-z A-Z)
export IDIR=$PWD/idir
export ETC=$IDIR/etc
export LC_ALL=C

################################################################################
debootstrap --arch amd64 wheezy $IDIR http://$APT_PROXY/ftp.us.debian.org/debian
mkdir $ETC/rc.backup && cp -a $ETC/rc?.d $ETC/rc.backup

mount -t proc   none $IDIR/proc
mount -t sysfs  none $IDIR/sys
mount -t devpts none $IDIR/dev/pts

mkdir -p $IDIR/tmp/user/0
ln -s ../proc/mounts $ETC/mtab

echo debian            > $ETC/hostname
echo 127.0.0.1 debian >> $ETC/hosts
chroot $IDIR hostname -F /etc/hostname

export APT="chroot $IDIR aptitude -y -vvv --allow-untrusted"

cat << EO_SRC_LIST > $IDIR/etc/apt/sources.list
deb http://$APT_PROXY/ftp.us.debian.org/debian wheezy main
deb http://$APT_PROXY/security.debian.org/ wheezy/updates main
EO_SRC_LIST

cat << EO_POLICY_RC > $IDIR/usr/sbin/policy-rc.d
#!/bin/sh
echo "All runlevel operations denied by policy"
exit 101
EO_POLICY_RC
chmod 755 $IDIR/usr/sbin/policy-rc.d

$APT update
export DEBIAN_FRONTEND=noninteractive

$APT install linux-image-amd64 "?name(firmware)" makedev squashfs-tools       \
    grub-legacy busybox-static ssh ntp ifplugd debootstrap apt-cacher sysstat \
    "?or(?section(otherosfs),?section(python),?section(database))"            \
    "?or(?section(gnome),?section(kde),?section(xfce))"                       \
    task-gnome-desktop task-kde-desktop task-xfce-desktop task-lxde-desktop   \
    kvm cgroup-bin virtualbox-guest-utils unetbootin htop                     \
    filezilla icedove midori minidlna xbmc terminator roxterm tmux            \
    task-web-server nginx squid gunicorn                                      \
    eclipse scala openjdk-7-jdk erlang lua5.2 golang                          \
    task-print-server task-file-server netcat bittorrent mutt wireshark       \
    "?or(?name(education),?name(sugar))=" command-not-found= iucode-tool=     \
    resolvconf= virtualenvwrapper=

cat << EO_SRC_LIST > $IDIR/etc/apt/sources.list
deb http://$APT_PROXY/ftp.us.debian.org/debian wheezy main non-free contrib
deb http://$APT_PROXY/security.debian.org/ wheezy/updates main non-free contrib
deb http://$APT_PROXY/dl.google.com/linux/chrome/deb/ stable main
deb http://$APT_PROXY/dl.google.com/linux/talkplugin/deb/ stable main
EO_SRC_LIST

$APT update
unset DEBIAN_FRONTEND

$APT install "?name(firmware)" google-chrome-stable google-talkplugin

$APT safe-upgrade
$APT clean

################################################################################
sed -i 's|root:[^:]*:|root:!:|'                             $ETC/shadow
cp $IDIR/usr/share/zoneinfo/Asia/Calcutta                   $ETC/localtime
echo "append domain-name-servers 208.67.222.222;"    >> $ETC/dhcp/dhclient.conf
echo "append domain-name-servers 208.67.220.220;"    >> $ETC/dhcp/dhclient.conf
echo "append domain-name-servers 8.8.8.8;"           >> $ETC/dhcp/dhclient.conf
echo "append domain-name-servers 8.8.4.4;"           >> $ETC/dhcp/dhclient.conf
sed -i '/^INTERFACES/s/=.*$/="eth0 eth1 eth2 eth3"/'    $ETC/default/ifplugd

echo 'export PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin' >> $ETC/bash.bashrc
echo 'export EDITOR=vim'                                 >> $ETC/bash.bashrc
echo -e "syntax on\nset hlsearch"                        >> $ETC/skel/.vimrc
echo -e "exec startlxde"                                  > $ETC/skel/.xinitrc

mkdir $ETC/skel/Desktop
cp $IDIR/usr/share/applications/google-chrome.desktop $ETC/skel/Desktop
cp $IDIR/usr/share/applications/roxterm.desktop       $ETC/skel/Desktop

cat << "EO_NETWORK_INTERFACES" > $ETC/network/interfaces
auto lo
iface lo inet loopback

iface eth0 inet dhcp
iface eth1 inet dhcp
iface eth2 inet dhcp
iface eth3 inet dhcp
EO_NETWORK_INTERFACES

mkdir -p $IDIR/tools && touch $IDIR/tools/vm && chmod 755 $IDIR/tools/vm
sed "s|VERSION|$VERSION|g" << "EO_KVM" > $IDIR/tools/vm
#!/bin/bash
[ $# -gt 0 ] && DISK=$1 || DISK=$(mktemp)
[ $# -gt 0 ] && DRIVE="-drive file=$DISK,if=virtio"
[ $# -gt 1 ] && NET="-net nic -net tap,ifname=$2,script=no"

BOOT="-kernel /boot/VERSION.kernel -initrd /boot/VERSION.initrd -append"
SQFS="-drive file=/boot/VERSION.sqfs,snapshot,if=virtio"
OPT="-m 512 -soundhw es1370 -vga vmware -daemonize"
flock -n $DISK -c "kvm $OPT $BOOT 'quiet Run' $SQFS $DRIVE"
EO_KVM

cat << "EO_BRIDGE" > $IDIR/tools/bridge && chmod 755 $IDIR/tools/bridge
#!/bin/bash
tunctl -u 5000 -t tap0
brctl addbr br0
brctl addif br0 eth0
brctl addif br0 tap0
ifconfig eth0 up 0.0.0.0
ifconfig tap0 up
dhclient br0
EO_BRIDGE

cat << "EO_RC_LOCAL" > $IDIR/etc/rc.local
#!/bin/bash

[ "Run" = $(cat /proc/cmdline | awk '{print $2}') ] || exit

if [ -f /boot/etc/authorized_keys ]; then
    mkdir -m 700 -p /root/.ssh
    cp /boot/etc/authorized_keys /root/.ssh
    dpkg-reconfigure openssh-server
fi

[ -f /boot/etc/password ] && /usr/sbin/usermod -p $(cat /boot/etc/password) user

for PART in $(cat /proc/partitions | awk '{print $4}'); do
    mkdir /media/$PART
    mount -o ro,nodev,nosuid /dev/$PART /media/$PART || rmdir /media/$PART

    REM=$(cat /sys/block/$(echo $PART | sed "s/[0-9]//g")/removable)

    U=/media/$PART/home
    [ -d $U ] && [ 5000 = $(stat -c %g $U) ] && export HOME_PART_$REM=$PART
done 2> /dev/null

[ $HOME_PART_0 ] && HOME_PART=$HOME_PART_0
[ $HOME_PART_1 ] && HOME_PART=$HOME_PART_1

if [ $HOME_PART ]; then
    HDIR=/media/$HOME_PART
    mount -o remount,rw,nodev,nosuid $HDIR && echo "Remounted $HDIR (rw)"
    mount --bind $HDIR/home /home && echo "Mounted $HDIR/home on /home"
fi

[ -x /boot/etc/rc.local ] && /boot/etc/rc.local
EO_RC_LOCAL

SSHD=$ETC/ssh/sshd_config
sed -i "/^#PasswordAuthentication /s|^.*$|PasswordAuthentication no|" $SSHD

rm $ETC/ssh/ssh_host_*_key $ETC/ssh/ssh_host_*_key.pub

ADDUSER="adduser --disabled-password" && rmdir $IDIR/home
chroot $IDIR $ADDUSER --uid 4000 --home /tmp   --gecos "Guest" guest
chroot $IDIR $ADDUSER --uid 5000 --home /home  --gecos "User"  user
chroot $IDIR adduser user sudo
chroot $IDIR adduser user vboxusers
echo -e "password\npassword" | chroot $IDIR passwd user
mv $IDIR/home $IDIR/home.rename

mkdir $ETC/rc.full && mv $ETC/rc?.d $ETC/rc.full
mv $ETC/rc.backup/* $ETC && rmdir $ETC/rc.backup
find $ETC/rc[2-5].d -type l -exec rm {} \;

for i in keyboard-setup qemu-kvm kbd console-setup x11-common; do
   chroot $IDIR update-rc.d $i start S
done

for i in ifplugd ntp rc.local; do
   chroot $IDIR update-rc.d $i defaults
done

TTYS0="T0:2345:respawn:/sbin/getty -a root -L ttyS0 38400 vt100"
sed -i "/ttyS0/s|^.*$|$TTYS0|" $ETC/inittab

sed -i '/#autologin-user=/s/^.*$/autologin-user=user/' $ETC/lightdm/lightdm.conf

WALLPAPER=/usr/share/images/desktop-base/spacefun-wallpaper.svg
ln -sf $WALLPAPER         $ETC/alternatives/desktop-background
ln -sf /usr/bin/startlxde $ETC/alternatives/x-session-manager
ln -sf /usr/bin/midori    $ETC/alternatives/x-www-browser
ln -sf /usr/bin/roxterm   $ETC/alternatives/x-terminal-emulator

mv $ETC/xdg/autostart $ETC/xdg/autostart.backup && mkdir $ETC/xdg/autostart
mv $ETC/xdg/autostart.backup/polkit-gnome-*    $ETC/xdg/autostart
mv $ETC/xdg/autostart.backup/nm-applet.desktop $ETC/xdg/autostart
mv $ETC/xdg/autostart.backup/gnome-sound-*     $ETC/xdg/autostart
mv $ETC/xdg/autostart.backup/gnome-keyring-*   $ETC/xdg/autostart
rm -f $ETC/X11/default-display-manager $ETC/X11/xorg.conf
################################################################################
cp $0 $IDIR/tools/wheezy

find $IDIR/var/cache/apt/archives -type f -exec rm -f {} \;
find $IDIR/var/log -type f -exec rm -f {} \;

rm -f $IDIR/usr/sbin/policy-rc.d
rm -f $IDIR/initrd.img $IDIR/vmlinuz

EXCLUDE="proc sys boot tmp root dev run media mnt selinux srv"
mksquashfs $IDIR $VERSION.sqfs -e $EXCLUDE
chmod 444 $VERSION.sqfs

################################################################################
rm -rf initrd boot
mkdir -p initrd/{proc,cgroup,sys,media,selinux,bb,lib,var,run,tmp}
mkdir -p initrd/dev/pts initrd/{rw,ro,delta} boot/etc

cp $IDIR/bin/busybox initrd/bb; for i in $(initrd/bb/busybox --list); do
    ln -s /bb/busybox initrd/bb/$i
done

cp -a $IDIR/lib/modules initrd/lib

(cd initrd/dev && MAKEDEV generic)
for i in $(seq 1 9); do
    MINOR=$((16*(i-1)))
    DISK=vd$(echo $i | tr 1-9 a-i)
    mknod initrd/dev/$DISK b 254 $MINOR
    for j in $(seq 1 15); do
        MINOR=$((16*(i-1)+j))
        PART=$DISK$j
        mknod initrd/dev/$PART b 254 $MINOR
    done
done

cat << "EO_BOOT_RC_LOCAL" > boot/etc/rc.local && chmod 755 boot/etc/rc.local
/etc/init.d/dbus restart
/etc/init.d/network-manager restart
/etc/init.d/lightdm restart
EO_BOOT_RC_LOCAL

sed "s|VERSION|$VERSION|g" << "EO_INIT" > initrd/init && chmod 755 initrd/init
#!/bb/busybox ash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/bb:$PATH

clear
mount -t proc   proc   /proc
mount -t sysfs  sysfs  /sys
mount -t cgroup cgroup /cgroup
echo 0 > /proc/sys/kernel/printk

[ "BusyBox" = $(cat /proc/cmdline | awk '{print $2}') ] && exec ash

modprobe -a sg usb-storage ehci_hcd ohci_hcd uhci_hcd xhci_hcd
modprobe -a sd_mod scsi_mod sr_mod
modprobe -a virtio virtio_blk virtio_net virtio_balloon virtio_ring
modprobe -a virtio_scsi virtio_pci
modprobe -a ahci
modprobe -a loop squashfs aufs ext3
sleep 7

for PART in $(cat /proc/partitions | awk '{print $4}'); do
    mkdir /media/$PART
    mount -o ro,nodev,nosuid /dev/$PART /media/$PART
    [ $? = 0 ] && echo "Mounted /dev/$PART (ro)" || rmdir /media/$PART

    REM=$(cat /sys/block/$(echo $PART | sed "s/[0-9]//g")/removable)

    [ -f /media/$PART/boot/VERSION.sqfs ] && export BOOT_PART_$REM=$PART
done 2> /dev/null

[ $BOOT_PART_0 ] && BOOT_PART=$BOOT_PART_0
[ $BOOT_PART_1 ] && BOOT_PART=$BOOT_PART_1

[ $BOOT_PART ] && ln -s /media/$BOOT_PART/boot /boot
[ $BOOT_PART ] && SQFS="-o loop /boot/VERSION.sqfs"
[ $BOOT_PART ] || SQFS="/dev/vda"

mount -t tmpfs tmpfs /delta
mount -t tmpfs tmpfs /tmp

mount -t squashfs -o ro $SQFS /ro && echo "Mounted $SQFS"
mount -t aufs -o dirs=/delta=rw:/ro=ro none /rw
mount --bind /rw/var /var

rm -rf /lib
for d in lib lib64 bin sbin tools; do
    ln -s /rw/$d /$d
done
for d in usr opt; do
    mkdir $d && mount --bind /rw/$d /$d
done

cp -a /ro/etc         /etc
cp -a /ro/home.rename /home

exec /sbin/init
EO_INIT

(cd initrd; find . | cpio -o -H newc | gzip > ../boot/$VERSION.initrd)
ln $IDIR/boot/vmlinuz-3.2.0-4-amd64 boot/$VERSION.kernel
ln $VERSION.sqfs boot/$VERSION.sqfs
cp -r $IDIR/usr/lib/grub/x86_64-pc boot/grub

sed "s|VERSION|$VERSION|g" << "EO_MENU_LST" > boot/grub/menu.lst
timeout 3
title Run     VERSION
    kernel /boot/VERSION.kernel quiet Run
    initrd /boot/VERSION.initrd
title Admin   VERSION
    kernel /boot/VERSION.kernel quiet Admin
    initrd /boot/VERSION.initrd
title BusyBox VERSION
    kernel /boot/VERSION.kernel quiet BusyBox
    initrd /boot/VERSION.initrd
EO_MENU_LST
