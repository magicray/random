#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <time.h>
#include <endian.h>
#include <sys/mman.h>
#include <openssl/md5.h>

#include "client.h"
#include "packet.h"
#include "debug.h"

char      role[64];
int       file_fd;
char     *file_map;
uint64_t  file_index;
uint64_t  file_size;
uint64_t  file_checked;
char      checksum[16];

typedef enum {
    DEFAULT=0,
    REPL_DST,
    REPL_SRC
} session_states;

struct session_state {
    session_states state;

    union {
        struct {
            uint32_t file;
            uint64_t offset;
            int      fd;
        } repl_src;
    } u;
};

typedef struct {
    uint8_t  prev_md5[16];
    uint16_t type;
    uint16_t length;
    uint8_t  buffer[USHRT_MAX + 16];
    uint8_t *md5;
} record;

int32_t read_record(int32_t fd, uint32_t offset, record *rec) {
    offset -= 16;
    int32_t length = pread(fd, rec, sizeof(record), offset);

    if(length < 1)  return -1;
    if(length < 36) return -2;

    if(length < (be16toh(rec->length) + 36))
        return -3;

    rec->md5 = &rec->buffer[be16toh(rec->length)];

    return 0;
}

uint64_t write_record(int fd, uint32_t length, char *buffer) {
    MD5_CTX  c;
    uint32_t len_be32 = htobe32(length);

    MD5_Init(&c);
    MD5_Update(&c, checksum, 16);
    MD5_Update(&c, &len_be32, 4);
    MD5_Update(&c, buffer, length);
    MD5_Final((unsigned char*)checksum, &c);

    struct iovec io[3] = {
        {&len_be32, 4},
        {buffer, length},
        {checksum, 16}
    };

    ASSERT((length+20) == writev(fd, io, 3));

    return length+20;
}

int open_file(uint32_t file_index, int32_t create) {
    char filepath[128];
    sprintf(filepath, "data/%u", file_index);
    
    return create ?
           open(filepath, O_RDWR|O_APPEND|O_CREAT, 0644) :
           open(filepath, O_RDWR|O_APPEND);
}

char *mmap_file(int fd, uint64_t begin, uint64_t end) {
    return (char*)mmap(0, end-begin, PROT_READ, MAP_PRIVATE, fd, begin);
}

uint64_t check_file(char *buf, uint64_t begin, uint64_t end, char *checksum) {
    ASSERT(begin <= end);

    while(begin < end) {
        unsigned char cksum[16];
        uint32_t length = be32toh(*(uint32_t*)(buf+begin));

        if((begin+length+20) > end)
            break;

        MD5((unsigned const char*)buf+begin-16, length+20, cksum);

        if(memcmp(buf+begin+4+length, cksum, 16)) {
            LOG("checksum mismatch at offset(%lu)", begin);
            break;
        }

        begin += length + 20;
    }

    memcpy(checksum, buf+begin-16, 16);
    return begin;
}

void repl_src_handler(void *conn, void *conn_state) {
    struct session_state *state = (struct session_state*)conn_state;
    uint32_t buffer_size = 10*1024*1024;

    char   *buf   = packet_connection_allocate_send_buf(conn, buffer_size);
    int64_t count = pread(state->u.repl_src.fd,
                          buf,
                          buffer_size,
                          state->u.repl_src.offset);
    if(count < 1) {
        record rec;
        int32_t ret = read_record(state->u.repl_src.fd,
                               state->u.repl_src.offset - 20,
                               &rec);
        if((0 == ret) && (0xFFFF == be16toh(rec.type))) {
            ALERT("socket(%d) repl_src file replicated %u:%u",
                conn, state->u.repl_src.file, state->u.repl_src.offset);
            state->state = DEFAULT;
            ASSERTZERO(close(state->u.repl_src.fd));
            return packet_handler_abort(conn);
        }
        packet_wait_event(conn, 0);
        LOG("socket(%d) waiting for event(0)", conn);
        return packet_handler_processing(conn);
    }

    state->u.repl_src.offset += count;

    LOG("socket(%d) repl_src sent(%lu)", conn, count);
    packet_connection_send(conn, REP_REPL_OK, count);
    return packet_handler_processing(conn);
}

void repl_dst_handler(void *conn, void *conn_state) {
    uint32_t req_type, req_length;
    char *req_buf = packet_connection_recv(conn, &req_type, &req_length);

    switch(req_type) {
        case REP_REPL_OK: {
            if(req_length < 1)
                break;

            ASSERT(req_length == write(file_fd, req_buf, req_length));
            file_size += req_length;

            if(file_size < 16)
                break;

            if(0 == file_checked)
                file_checked = 16;
                
            uint64_t verified = check_file(file_map,
                                           file_checked,
                                           file_size,
                                           checksum);
            if(*(uint32_t*)(file_map+verified) <= (file_size-verified)) {
                ASSERTZERO(ftruncate(file_fd, verified));
                ALERT("repl_dst original_size(%u) truncated(%u) final_size(%u)",
                    file_size, file_size-verified, verified);
                file_checked = verified;
                file_size    = verified;
                return packet_handler_abort(conn);
            }

            file_checked = verified;
            LOG("replicated(%lu) verified(%lu)", file_size, verified);
            ASSERT((file_checked + UINT_MAX + 20) > file_size);
        }   break;
        case CONNECTION_CLOSED: {
            ALERT("replication terminated by source. checked(%u) size(%u)",
                file_checked, file_size);

            if(file_checked == file_size) {
                record rec;
                if(0 == read_record(file_fd, file_size - 20, &rec)) {
                    if(0xFFFF == be16toh(rec.type)) {
                        ALERT("complete file received");
                        ASSERTZERO(fsync(file_fd));
                        ALERT("file %u synced", file_index);

                        file_index  += 1;
                        file_size    = 0;
                        file_checked = 0;
                    }
                }
            }
            ASSERTZERO(close(file_fd));
            file_fd = -1;
        }   break;
        default:
            return packet_handler_abort(conn);
    }
    return packet_handler_processing(conn);
}

void packet_handler(void *conn, void *conn_state) {
    struct session_state *state = (struct session_state*)conn_state;

    if(REPL_SRC == state->state)
        return repl_src_handler(conn, conn_state);

    if(REPL_DST == state->state)
        return repl_dst_handler(conn, conn_state);

    uint32_t req_type, req_length;
    char *req_buf, *rep_buf;

    if(0 == (req_buf = packet_connection_recv(conn, &req_type, &req_length)))
        return packet_handler_processing(conn);

    int64_t length;
    switch(req_type) {
        case REQ_ECHO:
            rep_buf = packet_connection_allocate_send_buf(conn, req_length);
            memcpy(rep_buf, req_buf, req_length);
            packet_connection_send(conn, REP_ECHO_OK, req_length);
            break;
        case REQ_APPEND:
        case REQ_PUT:
            if(file_size > UINT_MAX) {
                ASSERTZERO(fsync(file_fd));
                ASSERTZERO(close(file_fd));

                file_index += 1;
                file_size   = 16;

                file_fd = open_file(file_index, 1);
                ASSERT(16 == write(file_fd, checksum, 16));
            }

            length = write_record(file_fd, req_length, req_buf);

            if(REQ_PUT == req_type) {
                rep_buf = packet_connection_allocate_send_buf(conn, 12);
                *(uint32_t*)&rep_buf[0] = htobe32(file_index);
                *(uint32_t*)&rep_buf[4] = htobe32(file_size);
                *(uint32_t*)&rep_buf[8] = htobe32(req_length);

                packet_connection_send(conn, REP_PUT_OK, 12);
            }

            packet_trigger_event(0);
            file_size += length;
            break;
        case REQ_REPL:
            state->u.repl_src.file   = be32toh(*(uint32_t*)(&req_buf[0])); 
            state->u.repl_src.offset = be64toh(*(uint32_t*)(&req_buf[4])); 

            ALERT("socket(%d) replication request for %lu:%lu",
                conn, state->u.repl_src.file, state->u.repl_src.offset);

            if((state->u.repl_src.fd = open_file(state->u.repl_src.file, 0))<0)
                return packet_handler_abort(conn);

            state->state = REPL_SRC;
            packet_connection_send(conn, REP_REPL_OK, 0);
            break;
        default:
            return packet_handler_abort(conn);
    }

    return packet_handler_processing(conn);
}

void packet_loop_callback(void) {
    if((0 == strcmp("replicate", role)) && (file_fd < 0)) {
        static struct timespec old;
        struct timespec current;

        clock_gettime(CLOCK_REALTIME, &current);
        if(current.tv_sec <= old.tv_sec)
            return;

        old = current;

        char buf[12];
        *(uint32_t*)(&buf[0]) = htobe32(file_index);
        *(uint64_t*)(&buf[4]) = htobe64(file_size);

        void *state;
        if(0 != (state = packet_connect("127.0.0.1", 5000, REQ_REPL, 12, buf))){
            ASSERT((file_fd = open_file(file_index, 1)) >= 0);
            ASSERT((file_map = mmap_file(file_fd, 0, 3*UINT_MAX)) >= 0);
            ((struct session_state*)state)->state = REPL_DST;
            LOG("socket(%d) connection initiated", state);
        } else
            LOG("replication connection attampt failed");
    }
}

int main(int argc, char *argv[]) {
    DIR *dir;
    struct dirent *dent;

    ASSERTZERO(close(0));
    ASSERTZERO(close(1));
    ASSERTZERO(close(2));

    strcpy(role, argv[1]);

    bzero(checksum, 16);

    file_index = 0;

    ASSERT(dir = opendir("data"));
    while(dent = readdir(dir)) {
        uint32_t n = atol(dent->d_name);
        if(n > file_index)
            file_index = n;
    }

    if(file_index > 0) {
        struct stat s;
        ALERT("checking file : %u", file_index);
        ASSERT((file_fd = open_file(file_index, 0)) >= 0);
        ASSERTZERO(fstat(file_fd, &s));

        ASSERT((file_map = mmap_file(file_fd, 0, s.st_size)) >= 0);
        file_size = check_file(file_map, 16, s.st_size, checksum);
        ASSERTZERO(munmap(file_map, s.st_size));

        ASSERTZERO(ftruncate(file_fd, file_size));
        ALERT("original_size(%u) truncated(%u) final_size(%u)",
            s.st_size, s.st_size-file_size, file_size);
        ASSERTZERO(fsync(file_fd));
    }

    if(0 == strcmp("write", role)) {
        if(0 == file_index) {
            file_index = 1;
            file_size  = 16;
            bzero(checksum, 16);

            ASSERT((file_fd = open_file(file_index, 1)) >= 0);
            ASSERT(16 == write(file_fd, checksum, 16));
        }
    }

    if(0 == strcmp("replicate", role)) {
        if(0 == file_index) {
            file_index = 1;
            file_size  = 0;
        }
        file_checked = file_size;
        file_fd = -1;
    }

    packet_loop_run(atoi(argv[2]), sizeof(struct session_state));
}
