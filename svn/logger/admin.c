#include <stdio.h>
#include <string.h>

#include "client.h"
#include "debug.h"

int test_echo(int sock, int count, uint32_t length) {
    char *in  = (char*)malloc(length);
    char *out = (char*)malloc(length);

    LOG("TEST ECHO BEGIN");
    LOG("count = %d, length : %u", count, length);
    for(int i=0; i<count; i++) {
        memset(in, i, length);

        if(echo(sock, in, out, length))
            break;

        if(memcmp(in, out, length)) {
            LOG("memcmp failed. index : %d", i);
            return -1;
        }
    }
    LOG("TEST END");
    return 0;
}

int test_put(int sock, int count, uint32_t length) {
    char *in = (char*)malloc(length);
    char id[12];

    LOG("TEST PUT BEGIN");
    LOG("count = %d, length : %u", count, length);
    for(int i=0; i<count; i++) {
        memset(in, i, length);

        if(length != put(sock, in, length, id))
            break;

        uint32_t file_index   = be32toh(*(uint32_t*)&id[0]);
        uint32_t file_offset  = be32toh(*(uint32_t*)&id[4]);
        uint32_t block_length = be32toh(*(uint32_t*)&id[8]);

        LOG("%u %u %u", file_index, file_offset, block_length);
    }
    LOG("TEST END");
    return 0;
}

int32_t test_append(int32_t sock, int32_t count, uint16_t length) {
    char in[length];
    char id[10];

    LOG("TEST APPEND BEGIN");
    LOG("count = %d, length : %d", count, length);
    for(int i=0; i<count; i++) {
        memset(in, i, length);

        if(append(sock, in, length))
            break;
    }
    LOG("TEST END");
    return 0;
}

int main(int argc, char *argv[]) {
    int32_t sock = socket_connect(argv[1], atoi(argv[2]));

    if(0 == strcmp("test-echo", argv[3]))
        ASSERTZERO(test_echo(sock, atoi(argv[4]), atoi(argv[5])));

    if(0 == strcmp("test-put", argv[3]))
        ASSERTZERO(test_put(sock, atoi(argv[4]), atoi(argv[5])));

    if(0 == strcmp("test-append", argv[3]))
        ASSERTZERO(test_append(sock, atoi(argv[4]), atoi(argv[5])));

}
