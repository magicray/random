#!/usr/bin/python3
import sys, os, optparse, time
import logger

def test1(conf, args):
    with open(sys.argv[0]) as f:
        content = f.read()
    while True:
        logger.set_stream_name('stream_a')
        blob_id = logger.dump(content)
        logger.log("Hello : id1<%s> id2<%s>" % (blob_id, blob_id))

        logger.set_stream_name('stream_b')
        blob_id = logger.dump(content)
        logger.log("Hello : id1<%s> id2<%s>" % (blob_id, blob_id))

        time.sleep(1)

def test2(conf, args):
    (dirname,), x, y = args
    logger.set_stream_name('logtest2')

    for root, dirs, files in os.walk(dirname):
        dlink = logger.dump(dirs)
        flink = logger.dump(files)
        fmt   = "pwd(%s) dir_count(%d) dirs(%s) file_count(%d) files(%s)"
        logger.log(fmt % (root, len(dirs), dlink, len(files), flink))
        for filename in files:
            with open(os.path.join(root, filename)) as f:
                try:
                    flink = logger.dump(f.read())
                except UnicodeDecodeError:
                    flink = logger.dump("This file contains binary data")
            logger.log("File(%s) content(%s)" % (filename, flink))
