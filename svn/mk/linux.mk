KERNELORG     = http://www.kernel.org/pub
MPFR          = http://www.mpfr.org/mpfr-current
MPFR_VER      = 2.4.2
MPFR_TAR      = mpfr-${MPFR_VER}.tar.bz2
GMP           = ftp://ftp.gmplib.org/pub
GMP_VER       = 5.0.1
GMP_TAR       = gmp-${GMP_VER}.tar.bz2
GNUGNU        = http://ftp.gnu.org/gnu
GNUPUB        = http://ftp.gnu.org/pub
GNUNON        = http://ftp.gnu.org/non-gnu
SOURCEFORGE   = http://kent.dl.sourceforge.net/sourceforge
MYSQL_MIRROR  = http://mysql.he.net/Downloads
GNOME         = http://ftp.gnome.org/pub/GNOME/sources
GLIBC_VER     = 2.9
GCC_VER       = 4.4.3
BINUTILS_VER  = 2.20
NASM_VER      = 0.98.39
GCC_TAR       = gcc-${GCC_VER}.tar.bz2
BINUTILS_TAR  = binutils-${BINUTILS_VER}.tar.bz2
GLIBC_TAR     = glibc-${GLIBC_VER}.tar.bz2
GLIBC_THR_TAR = glibc-linuxthreads-${GLIBC_VER}.tar.bz2
PATH         := ${PWD}/bin:${PWD}/sbin:${PWD}/tmp/bin:${PWD}/tmp/sbin:$(PATH)

toolchain:
	######################## Temporary GCC and BINUTILS ###################

	mkdir -p src tmp
	cd src && wget ${GMP}/gmp-${GMP_VER}/${GMP_TAR}
	cd src && wget ${MPFR}/${MPFR_TAR}
	cd src && wget ${GNUGNU}/gcc/gcc-${GCC_VER}/${GCC_TAR}

	cd tmp && tar -jxvf ../src/${GMP_TAR}
	cd tmp/gmp-${GMP_VER} && ./configure --prefix=${PWD}/tmp
	make -C tmp/gmp-${GMP_VER} install
	rm -rf tmp/gmp-${GMP_VER}

	cd tmp && tar -jxvf ../src/${MPFR_TAR}
	cd tmp/mpfr-${MPFR_VER} && ./configure --prefix=${PWD}/tmp            \
		--with-gmp=${PWD}/tmp
	make -C tmp/mpfr-${MPFR_VER} install
	rm -rf tmp/mpfr-${MPFR_VER}

	cd tmp && tar -jxvf ../src/${GCC_TAR}
	mkdir tmp/g && cd tmp/g && ../gcc-$(GCC_VER)/configure                \
		--prefix=${PWD}/tmp --enable-languages=c --disable-werror     \
		--with-mpfr=${PWD}/tmp --with-gmp=${PWD}/tmp
	make -C tmp/g
	make -C tmp/g install
	rm -rf tmp/g

	cd src && wget ${GNUGNU}/binutils/${BINUTILS_TAR}
	cd tmp && tar -jxvf ../src/${BINUTILS_TAR}
	mkdir tmp/b && cd tmp/b && ../binutils-${BINUTILS_VER}/configure      \
		--prefix=${PWD}/tmp
	make -C tmp/b
	make -C tmp/b install
	rm -rf tmp/b

	################################## GLIBC ##############################
	cd src && wget ${GNUGNU}/glibc/${GLIBC_TAR}
	cd src && wget ${GNUGNU}/glibc/${GLIBC_THR_TAR}
	cd tmp && tar -jxvf ../src/${GLIBC_TAR}
	cd tmp && tar -jxvf ../src/${GLIBC_THR_TAR}
	mv tmp/linuxthreads    tmp/glibc-${GLIBC_VER}
	mv tmp/linuxthreads_db tmp/glibc-${GLIBC_VER}
	mkdir -p tmp/l usr/include
	cd tmp/l && ../glibc-${GLIBC_VER}/configure                           \
       	 --prefix=${PWD} --includedir=${PWD}/usr/include                      \
       	 --with-headers=${PWD}/usr/include                                    \
       	 --disable-profile --without-gd                                       \
       	 --without-selinux --enable-add-ons
	make -C tmp/l
	mkdir -p etc
	echo "${PWD}/lib"                         > etc/ld.so.conf
	echo "${PWD}/usr/lib"                    >> etc/ld.so.conf
	echo "${PWD}/usr/local/lib"              >> etc/ld.so.conf
	echo "${PWD}/X11/lib"                    >> etc/ld.so.conf
	env LANGUAGE=C LC_ALL=C make -C tmp/l install
	exit
	####################### Adjust temporary toolchain ####################
	duntar $GNUGNU/binutils/binutils-$BINUTILS_VER.tar.gz
	mkdir $SRC/binutils_build && cd $SRC/binutils_build
	../binutils-$BINUTILS_VER/configure --prefix=$IPATH/tmp
	make LIB_PATH=$BU_LIB && make install
	
	$IPATH/tmp/bin/gcc -dumpspecs > s1
	sed -i "s|/lib/ld-linux.so.2|$IPATH/lib/ld-linux.so.2|g" s1
	mv s1 $(dirname $($IPATH/tmp/bin/gcc -print-libgcc-file-name))/specs
	rm -rf $SRC

	########################## Final GCC and BINUTILS #####################
	duntar $GNUGNU/binutils/binutils-$BINUTILS_VER.tar.gz
	mkdir $SRC/binutils_build && cd $SRC/binutils_build
	PATH=$TMP ../binutils-$BINUTILS_VER/configure --prefix=$IPATH/usr
	PATH=$TMP make LIB_PATH=$BU_LIB && PATH=$TMP make install && rm -rf $SRC

	duntar $GNUGNU/gcc/gcc-$GCC_VER/gcc-$GCC_VER.tar.bz2
	mkdir $SRC/gcc && cd $SRC/gcc && PATH=$TMP ../gcc-$GCC_VER/configure \
       	 	--prefix=$IPATH/usr                                          \
	       	 --enable-languages=c,c++                                     \
       		 --enable-threads=posix                                       \
       		 --disable-werror                                             \
       		 --with-local-prefix=$IPATH/usr/local                         \
       		 --with-sysroot=$IPATH
	PATH=$TMP make && PATH=$TMP make install
	ln -s gcc $IPATH/usr/bin/cc -f
	$IPATH/usr/bin/gcc -dumpspecs > s
	sed -i "s|/lib/ld-linux.so.2|$IPATH/lib/ld-linux.so.2|g" s
	mv s $(dirname $($IPATH/usr/bin/gcc -print-libgcc-file-name))/specs
	rm -rf $SRC $IPATH/tmp

	################################## NASM ###############################
	mkdir -p $IPATH/usr/bin $IPATH/usr/man/man1
	build $KERNELORG/software/devel/nasm/source/nasm-$NASM_VER.tar.bz2

	####################### GNU make and associated utilities #############
	build $GNUGNU/m4/m4-1.4.4.tar.gz
	build $GNUGNU/autoconf/autoconf-2.59.tar.bz2
	build $GNUGNU/automake/automake-1.9.6.tar.bz2
	build $GNUGNU/gettext/gettext-0.14.5.tar.gz
	build $GNUGNU/bison/bison-2.1.tar.bz2
	build $GNUGNU/make/make-3.80.tar.bz2
	build http://pkgconfig.freedesktop.org/releases/pkgconfig-0.18.tar.gz \
	  "--with-pc-path=$IPATH/usr/lib/pkgconfig:$IPATH/X11/lib/pkgconfig"   

	####################### Everything DONE. HuH... #######################
	strip_bin_lib
