import time, os

LOGPATH = os.environ.get('TOOLBOX_LOGPATH', '/tmp/logs')

def stream_content(ctx, args):
    (appid, streamid), query, data = args

    log_path = os.path.join(LOGPATH, appid, 'streams')

    if ('p' in query) and ('l' in query):
        p = int(query['p'][0])
        l = int(query['l'][0])

        with open(os.path.join(log_path, streamid + '.blob')) as f:
            f.seek(p)
            content = f.read(l) 
        return ('OK', content)
    else:
        with open(os.path.join(log_path, streamid)) as f:
            content = f.read()
        return ('OK', '<table align="center" border="1">' +content+ '</table>')

def stream_list(ctx, args):
    (appid, streamid), query, data = args

    log_path = os.path.join(LOGPATH, appid)

    mode = 'all' if '' == streamid else 'single'

    result = ""
    utc_usec = time.time()
    for i in range(2):
        gmtime = time.gmtime(utc_usec - i*86400)
        timestamp = time.strftime("%y%m%d", gmtime)
        indexdir  = os.path.join(log_path, 'index', timestamp)
        if not os.path.isdir(indexdir):
            continue

        filelist  = [f for f in os.listdir(indexdir)]
        for f in filelist:
            filepath = os.path.join(indexdir, f)
            if not os.path.isfile(filepath):
                continue

            index_contents = list()
            with open(filepath) as index_file:
                index_contents.extend(index_file.readlines())
                
        if 'all' == mode:
            streams = set([l.split('/')[-2] for l in index_contents])
            for s in streams:
                result  += "<tr><td><a href=\"%s\">%s</a></td></tr>" % (s, s)
        elif 'single' == mode:
            fmt = "<td>%s</td><td><a href=\"%s\">%s</a></td><td>%s</td>"
            for l in index_contents:
                f = l.split('/')

                if streamid != f[-2]:
                    continue

                session  = '.'.join(f[-1].split('.')[0:3])
                hostname = '.'.join(f[-1].split('.')[3:])

                result += "<tr>" +(fmt % (streamid,l,session,hostname))+ "</tr>"

    return ('OK', '<table align="center" border="1">' + result + '</table>')

WS_YAML = """
-
  regex:    ^(\w+)/([^/]*)$
  GET:
    method: stream_list
-
  regex:    ^(\w+)/streams/(.+)$
  GET:
    method: stream_content
"""
