#!/usr/bin/python
import optparse, logger, sqlclient, sqlite3, sys, time

qlist = dict(
    get_indicators = [
        "select * from prices where stock=:stock and date=:date"
    ],
    min_date   = [ "select min(date) as date from prices where stock=:stock" ],
    get_dates  = [ "select distinct date from prices order by date" ],
    stock_list = [
        """select stock from prices
           where date=20120514
           order by volume desc limit 1000""" ]
)
#where date=19951010
#where date=20120514

class Portfolio(object):
    def __init__(self, cash, stocks, db):
        self.cash     = cash
        self.stocks   = stocks
        self.trades   = []
        self.db       = db

    def value(self):
        value = 0

        for trade in self.trades:
            if trade['state'] not in ('LOSS', 'PROFIT'):
                value += trade['value']

        return value

    def update(self, date):
        for trade in self.trades:
            records = self.db.get_indicators(stock=trade['stock'], date=date)
            if 0 == len(records):
                continue

            close  = records[0].close

            trade['date']  = date
            trade['close'] = close
            trade['value'] = trade['quantity'] * close

            signal = self.exit_signal(trade, records[0])
            if True == signal:
                self.cash += trade['quantity'] * close
                self.close_trade(trade, date, close)
        
        for s in self.stocks:
            min_date = self.db.min_date(stock=s)[0].date
            if (None == min_date) or (date < min_date + 10000):
                continue

            records = self.db.get_indicators(stock=s, date=date)
            if 0 == len(records):
                continue

            signal = self.entry_signal(records[0])
            close = records[0].close
            if True == signal:
                owned_stocks = []
                for t in self.trades:
                    if t['state'] not in ('LOSS', 'PROFIT'):
                        owned_stocks.append(t['stock'])

                if (self.cash > self.lot_size()) and (s not in owned_stocks):
                    quantity = int(self.lot_size()/close)
                    if quantity > 0:
                        self.cash -= quantity*close
                        self.open_trade(s, date, close, quantity)

class YearlyBreakoutPortfolio(Portfolio):
    def __init__(self, cash, stocks, db):
        Portfolio.__init__(self, cash, stocks, db)

    def lot_size(self):
        return (self.value() + self.cash) / 10

    def entry_signal2(self, r):
        filter_1 = (r.close > r.high120)
        filter_2 = (r.ma10 > r.ma60) and (r.ma60 > r.ma120)
        filter_3 = (r.close < 1.5 * r.low120)

        return filter_1 and filter_2 and filter_3

    def entry_signal(self, r):
        filter_1 = (r.close >= r.high250)
        filter_2 = (r.ma120 > r.ma250) and (r.ma10 > r.ma120)
        filter_3 = True#(130 > r.roc250 > 120) and (120 > r.roc120 > 110)
        filter_4 = True#(r.volume > r.volma250)

        #filter_3 = r.rsi60 > 70

        #filter_1 = (r.close >= r.high120)
        #filter_2 = (r.high250 < r.close * 1.3) and (r.high250 > r.close * 1.1)
        #filter_3 = (r.close > r.ma60) and (r.ma60 > r.ma120)
        #filter_4 = (r.volume > r.volma60)

        return filter_1 and filter_2 and filter_3 and filter_4

    def exit_signal(self, trade, r):
        if ('PROFIT' == trade['state']) or ('LOSS' == trade['state']):
            return False

        if r.close > trade['max_price']:
            trade['max_price'] = r.close

        if (r.close > 1.33 * trade['buy_price']):
            return True

        if (r.close < 0.75 * trade['max_price']):
            return True

        return False

    def open_trade(self, stock, date, price, quantity):
        self.trades.append(dict(stock=stock, state='LONG', quantity=quantity,
            buy_date=date, buy_price=price, max_price=price,
            close=price,value=price*quantity,date=date))

        tup = ('LONG', stock, date, price, quantity)

    def close_trade(self, trade, date, price):
        trade['sell_date']  = date
        trade['sell_price'] = price
        if price > trade['buy_price']:
            trade['state'] = 'PROFIT'
            diff = trade['quantity'] * (price - trade['buy_price'])
        else:
            trade['state'] = 'LOSS'
            diff = trade['quantity'] * (trade['buy_price'] - price)

        tup  = (trade['state'], trade['stock'], date, trade['buy_price'],
                trade['sell_price'], trade['quantity'], diff)
        print("%8s : %10s %d %10.2f %10.2f %6d %10.2f" % tup)

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('--stocks', dest='stocks', help='stock symbols')
    parser.add_option('--db',     dest='db',     help='database')
    options, args = parser.parse_args()

    db = sqlclient.SQLClient(sqlite3.connect(options.db), qlist)

    if None == options.stocks:
        stock_list = [s.stock for s in db.stock_list()]
    else:
        stock_list = options.stocks.upper().split(',')

    portfolio = YearlyBreakoutPortfolio(100000, stock_list, db)

    avg_cash   = 0
    nDays      = 0
    current_yr = 1996
    for d in db.get_dates():
        portfolio.update(d.date)
        avg_cash += portfolio.cash
        nDays    += 1
        if (int(d.date / 10000)) > current_yr:
            current_yr = int(d.date / 10000)
            print("-" * 80);
            print("  Total = %10.2f" % (portfolio.value() + portfolio.cash));
            print("-" * 80);
    avg_cash = avg_cash / nDays

    print("Cash   : %10.2f" % (portfolio.cash))
    profit = loss = total = 0
    for p in portfolio.trades:
        total += 1
        state = p['state']
        if 'LONG' == state:
            print('%10s %d %d' % (p['stock'], p['quantity'], p['buy_date']))
        if 'PROFIT' == state:
            profit += 1
        if 'LOSS' == state:
            loss += 1
    print("Total : %d Profit : %d Loss %d" % (total, profit, loss))
    print("Avg Cash : %f" % (avg_cash))
    print("Stock value : %f" % (portfolio.value()))
