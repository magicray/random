import os, sys, http.server, yaml, urllib.parse, re

APPSPATH = os.environ.get('CLOUD_APPSPATH', '/media/sda1/svn/apps/')

def get_service(module, http_method, path):
    if '' == path:
        raise Exception('NO_RESOURCE_REQUESTED')

    matching_services = list()
    for ws in yaml.load(module.WS_YAML):
        m = re.search(ws['regex'], path)
        if m:
            matching_services.append((ws, m.groups()))

    match_len = len(matching_services)

    if 0 == match_len:
        raise Exception('INVALID_SERVICE: ' + path)

    if 1 < match_len:
        raise Exception("MULTIPLE_MATCHES: (%d) %s" % (match_len, path))

    return (matching_services[0][0][http_method], matching_services[0][1])

class WebReqHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        try:
            url      = urllib.parse.urlparse(self.path)
            appid    = url.path.split('/')[1]
            resource = '/'.join(url.path.split('/')[2:])
            query    = urllib.parse.parse_qs(url.query)

            sys.path.append(os.path.join(APPSPATH, appid))

            module    = __import__('app')
            ws, regex = get_service(module, 'GET', resource)
            method    = getattr(module, ws['method'])

            status, result = method(None, (regex, query, None))
        except ImportError as e:
            result = "INVALID_APP"
        except Exception as e:
            result = str(e)

        self.wfile.write(result.encode('utf8'))

def run(ip, port):
    httpd = http.server.HTTPServer((ip, port), WebReqHandler)
    httpd.serve_forever()
