#!/usr/bin/python
import logger, sqlclient, sqlite3, os, time, zipfile, io, optparse

qlist = dict()
qlist['schema_init'] = [
    """create table if not exists prices (date int, stock text,
       open float, high float, low float, close float, volume float,

       low5     float, ma5     float, high5    float,
       roc5     float, rsi5    float, volma5   float,
       padi5    float, nadi5   float, tr5      float,

       low10    float, ma10    float, high10   float,
       roc10    float, rsi10   float, volma10  float,
       padi10   float, nadi10  float, tr10     float,

       low20    float, ma20    float, high20   float,
       roc20    float, rsi20   float, volma20  float,
       padi20   float, nadi20  float, tr20     float,

       low60    float, ma60    float, high60   float,
       roc60    float, rsi60   float, volma60  float,
       padi60   float, nadi60  float, tr60     float,

       low120   float, ma120   float, high120  float,
       roc120   float, rsi120  float, volma120 float,
       padi120  float, nadi120 float, tr120    float,

       low250   float, ma250   float, high250  float,
       roc250   float, rsi250  float, volma250 float,
       padi250  float, nadi250 float, tr250    float,

       primary key(date, stock))""",
    "create index if not exists prices_stock_date on prices(stock, date)",
    "create index if not exists prices_stock on prices(stock)",
    "create index if not exists prices_date  on prices(date)"
]

qlist['insert_prices'] = [
    """insert into prices values(:date,:stock,:open,:high,:low,:close,:volume,
       null, null, null, null, null, null, null, null, null,
       null, null, null, null, null, null, null, null, null,
       null, null, null, null, null, null, null, null, null,
       null, null, null, null, null, null, null, null, null,
       null, null, null, null, null, null, null, null, null,
       null, null, null, null, null, null, null, null, null)
    """
]

qlist['update_indicators'] = [
    """update prices set
       low5=:low5,       ma5=:ma5,         high5=:high5,
       roc5=:roc5,       rsi5=:rsi5,       volma5=:volma5,
       padi5=:padi5,     nadi5=:nadi5,     tr5=:tr5,

       low10=:low10,     ma10=:ma10,       high10=:high10,
       roc10=:roc10,     rsi10=:rsi10,     volma10=:volma10,
       padi10=:padi10,   nadi10=:nadi10,   tr10=:tr10,

       low20=:low20,     ma20=:ma20,       high20=:high20,
       roc20=:roc20,     rsi20=:rsi20,     volma20=:volma20,
       padi20=:padi20,   nadi20=:nadi20,   tr20=:tr20,

       low60=:low60,     ma60=:ma60,       high60=:high60,
       roc60=:roc60,     rsi60=:rsi60,     volma60=:volma60,
       padi60=:padi60,   nadi60=:nadi60,   tr60=:tr60,

       low120=:low120,   ma120=:ma120,     high120=:high120,
       roc120=:roc120,   rsi120=:rsi120,   volma120=:volma120,
       padi120=:padi120, nadi120=:nadi120, tr120=:tr120,

       low250=:low250,   ma250=:ma250,     high250=:high250,
       roc250=:roc250,   rsi250=:rsi250,   volma250=:volma250,
       padi250=:padi250, nadi250=:nadi250, tr250=:tr250

       where stock=:stock and date=:date"""
]

qlist['row_count']     = [
    "select count(*) as count from prices where date=:date"
]

qlist['get_date_list'] = [
    "select distinct(date) from prices order by date"
]

qlist['stock_list_for_date'] = [
    "select stock from prices where date=:date"
]

qlist['price_volume'] = [
    """select * from prices
       where  stock=:stock and date <= :date
       order  by date desc limit :period"""
]

qlist['check_indicators_for_date'] = [ 
    """select count(*) as count
       from prices where date=:date and low5 is not null"""
]

def insert_prices(sql, data_dir):
    log   = logger.Logger('insert_prices')
    start = int(time.mktime((1995,1,2,0,0,0,0,0,0)))

    for i in range(int((int(time.time()) - start)/86400)):
        today = time.localtime(start + i*86400)
        dd    = time.strftime("%d", today)
        mm    = time.strftime("%m", today).upper()
        yyyy  = time.strftime("%Y", today)
        date  = "%s%s%s" % (yyyy, mm, dd)

        if sql.row_count(date=date)[0].count > 0:
            log.out("Already inserted in database for %s" % (date))
            continue

        try:
            p    = open(os.path.join(data_dir, date), 'rb').read()
            q    = zipfile.ZipFile(io.BytesIO(p))
            data = str(q.read(q.namelist()[0])).split(',')
            log.out('Inserted in database. date(%s)' % (date))
        except Exception as e:
            h = log.blob(e)
            log.out('Insert failed. date(%s) exception(%s)' % (date, h))
            continue

        bhavcopy_column_count = 11
        if ('TOTALTRADES' == data[11]) and ('ISIN' == data[12]):
            bhavcopy_column_count = 13

        for i in range(len(data)//bhavcopy_column_count):
            j = i*bhavcopy_column_count
            if data[j+10] == 'TIMESTAMP':
                continue

            row              = dict()
            row['stock']     = data[j+0].strip().lstrip('\\n')
            row['open']      = data[j+2].strip()
            row['high']      = data[j+3].strip()
            row['low']       = data[j+4].strip()
            row['close']     = data[j+5].strip()
            row['last']      = data[j+6].strip()
            row['volume']    = data[j+9].strip()
            date             = data[j+10].strip()

            try:
                date=int(time.strftime("%Y%m%d",time.strptime(date,"%d-%b-%Y")))
                row['date'] = date
                sql.insert_prices(**row)
            except Exception as e:
                t = (date, log.blob(row), log.blob(e))
                log.out('Insert failed. date(%s) row(%s) exception(%s)' % t)
        sql.commit()

def compute_indicators(sql):
    log  = logger.Logger('compute_indicators')
    for d in sql.get_date_list():
        if sql.check_indicators_for_date(date=d.date)[0].count > 0:
            log.out('Already calculated for date(%d)' % (d.date))
            continue

        for s in sql.stock_list_for_date(date=d.date):
            row = dict(date=d.date, stock=s.stock)
            try:
                rows = sql.price_volume(stock=s.stock, date=d.date, period=250)

                for n in [5, 10, 20, 60, 120, 250]:
                    s                = str(n)
                    c                = [ z.close  for z in rows[0:n]]
                    v                = [ z.volume for z in rows[0:n]]
                    last             = c[0]
                    prev             = c[-1]
                    row['high'  + s] = max(c)
                    row['low'   + s] = min(c)
                    row['volma' + s] = sum(v)/min(n, len(v))
                    row['ma'    + s] = sum(c)/min(n, len(c))
                    row['roc'   + s] = int(100.5 + ((last-prev)*100)/prev)

                    up   = 0
                    down = 0
                    for i in range(len(c)-1):
                        if int(c[i]) > int(c[i+1]):
                            up += c[i] - c[i+1]
                        if int(c[i]) < int(c[i+1]):
                            down += c[i+1] - c[i]

                    row['rsi' +s] = int((100*up)/(up+down+0.0000001))

                    padi = nadi = tr = 0
                    for i in range(len(c)-1):
                        tr += max((rows[i].high-rows[i].low),
                                  abs(rows[i].high-rows[i+1].close),
                                  abs(rows[i].low-rows[i+1].close))

                        padi += max(0, rows[i].high - rows[i+1].high)
                        nadi += max(0, rows[i+1].low - rows[i].low)

                    row['padi' + s] = padi / len(c) 
                    row['nadi' + s] = nadi / len(c) 
                    row['tr'   + s] = tr   / len(c) 

                sql.update_indicators(**row)
            except IndexError:
                log.out('Exception(IndexError) for %s : %d'%(s.stock, d.date))
                pass
        log.out('Calculated for date(%d)' % (d.date))
        sql.commit()

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('--db',       dest='db',       help='database')
    parser.add_option('--data-dir', dest='data_dir', help='data dir')
    parser.add_option('--action',   dest='action',   help='action')
    options, args = parser.parse_args()

    sql = sqlclient.SQLClient(sqlite3.connect(options.db), qlist)
    sql.schema_init()

    if 'prices' == options.action:
        insert_prices(sql, options.data_dir)
    if 'indicators' == options.action:
        compute_indicators(sql)
