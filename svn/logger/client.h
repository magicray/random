#ifndef __CLIENT_H__
#define __CLIENT_H__
#include <stdint.h>
#include <limits.h>

typedef enum {
    INVALID=0,

    REQ_ECHO,
    REP_ECHO_OK,
    REP_ECHO_FAIL,

    REQ_GET,
    REP_GET_OK,
    REP_GET_FAIL,

    REQ_PUT,
    REP_PUT_OK,
    REP_PUT_FAIL,

    REQ_APPEND,
    REP_APPEND_OK,
    REP_APPEND_FAIL,

    REQ_SYNC,
    REP_SYNC_OK,
    REP_SYNC_FAIL,

    REQ_REPL,
    REP_REPL_OK,
    REP_REPL_FAIL,

    CONNECTION_CLOSED
} PACKET_TYPE;

int socket_connect(const char *ip, int32_t port);
uint32_t socket_send(int sock, uint32_t type, uint32_t length, char* buffer);
int echo(int sock, char *in, char *out, uint32_t length);
int append(int sock, char *buf, uint32_t len);
int put(int sock, char *buf, uint32_t len, char *id);
#endif
