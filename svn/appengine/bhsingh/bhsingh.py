from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import urlfetch
from google.appengine.api import mail
import re

def LastStockPrice(symbol):
    url = "http://www.nasdaq.com/aspx/flashquotes.aspx?symbol=" + symbol
    return re.search('<label id="....lastsale">\$([.0-9]+)</label>',
                     urlfetch.fetch(url).content).groups()[0]

class MSFTStockPrice(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/xml'
        msft = LastStockPrice('MSFT')
        msg  = '<Price Stock="Microsoft">' + str(msft) + '</Price>'
        addresses = ["parikshit.gupta@microsoft.com",
                     "jayantk@microsoft.com",
                     "bhupendra.singh@microsoft.com"]
        for addr in addresses:
            subject = "Microsoft stock Price is : $" + str(msft)
            body = "Sent by a fun program hosted at Google AppEngine"
            mail.send_mail("bhsingh@gmail.com", addr, subject, body)
        self.response.out.write(msg)

application = webapp.WSGIApplication([('/msftstockprice', MSFTStockPrice)],
                                     debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
